(require 'monitor)




(defun snmp-go ()
  (let ((value nil))
    (message "%S" (mapcar (lambda (oid)
			    (setq value 
				  (snmp-get snmp-host
					    snmp-community-string
					    (first oid)))
			    (rrd-update (second oid) (second value)))
			  snmp-oids))))

(setq snmp-timer (run-at-time t 300 'snmp-go))

;(snmp-go)

; (cancel-timer snmp-timer)

(while t
  (sit-for 5))

