;;; whiteboard.el --- chat and share files and buffers with other
;;                    users of emacs

;; Copyright (C) 2006  David O'Toole

;; Author: David O'Toole <dto@gnu.org>
;; Keywords: hypermedia, multimedia
;; $Id: whiteboard.el,v 1.36 2006/05/12 21:28:52 dto Exp dto $

;; This file is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 2, or (at your option)
;; any later version.

;; This file is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with GNU Emacs; see the file COPYING.  If not, write to
;; the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
;; Boston, MA 02110-1301, USA.

;;; Commentary:

;;;; About:
;;
;; Whiteboard.el allows two or more emacs users to chat, share files,
;; and share buffers over the internet.
;;
;; Requires the rsync frogram to transfer files.  
;;
;; This is an experimental and immature program. Use at your own risk.

;;;; Configuration: 
;;
;; Add (require 'whiteboard) to your emacs initialization file. 
;;
;; Before using Whiteboard you must set several variables to
;; acceptable values:
;;
;; (setq wb-directory "~/whiteboard")
;; (setq wb-base-port 10191)
;; (setq wb-local-ip "MY.IP.ADDRESS")
;;
;; If you are behind a NAT (network address translation) box, and
;; using port forwarding for Whiteboard, be sure to open both
;; wb-base-port and wb-base-port + 1. The latter is used for file
;; transfer. The variable wb-local-ip should be set to the IP address
;; of either your machine, or the externally-visible address of the
;; NAT box (in the case of port forwarding).

;;;; Usage: 
;;
;; The server must be started first, with M-x wb-make-server. The name
;; should be the name of the server, usually your username. This name
;; will appear as your "nick" during chat.
;;
;; The other user should start the client with M-x wb-make-client.
;;
;; The buffer *wb-console* can be used for chatting, much like an IRC
;; client. 
;;
;; There is also special buffer called "whiteboard" that you can work
;; on together. You take turns editing by locking the whiteboard with
;; C-c C-c (wb-get-or-release-lock), editing the whiteboard text, then
;; unlocking it again with C-c C-c. While you have locked the
;; whiteboard, the other user's whiteboard will be set to read-only,
;; but you can still chat and share files. 
;;
;; You may share files and other buffers with the following commands: 
;;
;; M-x wb-share-file             filename must be in your wb-directory
;; M-x wb-share-buffer           send a buffer to the other user
;; M-x wb-share-current-buffer   send current buffer
;;
;; You may end the session with: 
;;
;; M-x wb-terminate              end session
;;
;; When a file, buffer, or URL appears in the *wb-console* buffer, you
;; may RET or Mouse-2 the notification to open/download it.
;;
;; You can navigate through such buttons in the console with C-c C-b
;; (backward-button) and C-c C-f (forward-button).

; (setq wb-base-port 10191)
; (setq wb-local-ip "66.189.73.146")
; (wb-make-server "dto")

; (setq wb-base-port 10191)
; (setq wb-local-ip "192.168.1.52")
; (wb-make-client "zanac" "192.168.1.20")

;;; Code:

(require 'button)
(eval-when-compile 
  (require 'cl))


;;;; Variables for the user to set before starting a node


(defvar wb-directory "~/whiteboard" "Directory for whiteboard shared files.")
(defvar wb-local-ip nil "IP that other nodes can use to contact
you. If you are behind a NAT box with port forwarding, set this
to the externally visible address.")
(defvar wb-base-port 50101 "Base port for connections.")


;;;; Console 


(defvar wb-console nil "Buffer where console messages are printed.")


(defun wb-make-console ()
  "Create the console buffer and set up keybindings."
  (setq wb-console (get-buffer-create "*wb-console*"))
  (switch-to-buffer wb-console)
  (whiteboard-mode 1)
  (local-set-key (kbd "RET") 'wb-console-ret))


(defun wb-console-insert (string &optional echo)
  "Insert a string into the console without disturbing user input.
If ECHO is t, then also echo message in the echo area."
  (let ((oldbuf (current-buffer))
	(was-typing-p nil)
	(position-in-typing-line nil))
    (with-current-buffer wb-console
      ;; kill line of text if user was typing
      (when (and (not (bolp)) (last-line-of-buffer-p))
	(setq was-typing-p t)
	(setq position-in-typing-line (- (point) (point-at-bol)))
	(end-of-line)
	(let ((end (point)))
	  (beginning-of-line)
	  (kill-region (point) end)))
      (goto-char (point-max))
      ;;
      (insert string)
      (when echo
	(message string))
      ;;
      (insert "\n")
      ;; put text back if user was typing
      (when was-typing-p
	(yank)
	(goto-char (+ (point-at-bol) position-in-typing-line))))))


(defun wb-console-notify (string)
  "Insert highlighted text of STRING into console, and into echo area."
  (wb-console-insert (propertize string 'face font-lock-warning-face) t))


(defun wb-console-ret ()
  "Handle RET pressed in console buffer.  This way, you can both
press buttons and send chat messages, depending on location of
point."
  (interactive)
  (if (button-at (point))
      (push-button)
    (if (last-line-of-buffer-p)
	(wb-console-send))))


(defun wb-console-send ()
  "Send current typed message to other node."
  (when (not (bolp))
    (let (end (point))
      (beginning-of-line)
      (re-search-forward "\\(.*$\\)")
      (wb-message wb-process :action "chat" :nick wb-nick :text (match-string 1))
      (insert "\n"))))


(defun button-to-string (label action)
  "Create a button with label LABEL action ACTION to a string.
Useful for inserting buttons into the console with
`wb-console-insert'"
  (with-temp-buffer
    (insert-text-button label 'action action)
    (buffer-substring (point-min) (point-max))))


(defun last-line-of-buffer-p ()
  (save-excursion
    (end-of-line)
    (eobp)))


;;;; Server user management


(defvar wb-users nil "List of process objects for currently connected users.")

(defun wb-add-user (process)
  (setq wb-users (append wb-users (list process))))

(defun wb-remove-user (process)
  (setq wb-users (delq process wb-users)))

(defun wb-set-user-property (process key value)
  (process-put process key value))

(defun wb-get-user-property (process key)
  (process-get process key))

 
;;;; Building and connecting network nodes


(defvar wb-process nil "Process for local network node.")
(defvar wb-nick nil "Name for local network node.")
(defvar wb-is-server-p nil "Whether or not I am the server.")


(defun wb-setup-directory ()
  "Create whiteboard directory, if it does not already exist."
  (unless (file-exists-p wb-directory)
    (make-directory wb-directory)))


(defun wb-make-common ()
  "Parts common to making client and server nodes."
  (setq wb-rsync-checked nil
	wb-locked nil
	wb-global-lock nil
	wb-packet-queue nil
	wb-message-remain 0
	wb-message-stringbuf nil
	wb-users nil)
  ;;
  (wb-make-console)
  (wb-make-whiteboard)
  (wb-setup-directory)
  (wb-configure-rsync)
  (wb-start-rsync))


(defun wb-make-server (nick)
"Start server named NICK."
  (interactive "sServer name:\n")
  ;;
  (wb-make-common)
  (setq wb-nick nick)
  (setq wb-is-server-p t)
  ;;
  (let ((local (vector 0 0 0 0 wb-base-port)))
    (setq wb-process
	  (make-network-process :name "wb-server" 
				:local local
				:server t
				:service wb-base-port
				:filter 'wb-process-filter
				:sentinel 'wb-process-sentinel
				:buffer wb-console)))
  (when (not wb-process)
    (error "Failed to make server.")))


(defun wb-make-client (nick host)
  "Start client with nick NICK and connect to host HOST."
  (interactive "sClient name: \nsServer IP:\n")
  ;;
  (wb-make-common)
  (setq wb-nick nick)
  (setq wb-process
	(make-network-process :name "wb-client" 
			      :host host 
			      :service wb-base-port
			      :filter 'wb-process-filter
			      :sentinel 'wb-process-sentinel
			      :buffer wb-console))
  ;; send identification
  (wb-message wb-process :action "identify" :nick nick))


(defun wb-terminate ()
  "Terminate whiteboard process."
  (interactive)
  ;;
  (if wb-is-server-p
      (mapcar (lambda (p)
		(delete-process p))
	      wb-users))
  (delete-process wb-process)
  (kill-buffer wb-whiteboard)
  (kill-buffer wb-console)
  (wb-kill-rsync)
  (setq wb-process nil))


;;;; Sending events


(defun wb-message (from-process &rest properties)
  "Send property list message to other nodes.  FROM-PROCESS
should be the process sending the message. PROPERTIES are the
property/value pairs to send."
  ;; prefix length as 10 digits to plist
  (let* ((propstring (format "%S" properties))
	 (lenstring (format "%010d" (length propstring)))
	 (message (concat lenstring propstring)))
    ;;
    (if wb-is-server-p 
	;;
	;; broadcast when server
	(mapcar (lambda (p)
		  ;; don't repeat message to same node it came from
		  (unless (equal p from-process)
		    (process-send-string p message)))
		wb-users)
      ;;
      ;; send to server when client
      (process-send-string wb-process message))))
			    

(defun wb-reflect (process properties)
  "Reflect message from one node to other nodes.
Only the server should call this function."
  (when wb-is-server-p
    (eval `(wb-message ,process ,@properties))))
		

;;;; Receiving events


(defvar wb-global-lock nil
  "Used to deal with race conditions in emacs.")

(defvar wb-message-remain 0
  "Total remaining input to be buffered in this propstring.")

(defvar wb-message-stringbuf nil
  "Location to store buffered propstring.")

(defvar wb-packet-queue nil
  "List of strings used to buffer entire incoming packets.")


(defun wb-queue-packet (packet)
  (nconc wb-packet-queue (list packet)))


(defun wb-unqueue-packet ()
  (pop wb-packet-queue))


(defun wb-handle-packet (packet)
"Process PACKET by extracting property string.  
Returns propstring, if complete. If propstring is not complete, return nil, and
keep buffering partial propstring."
    (let ((message-length nil)
	  (packet-length (length packet))
	  (propstring nil))
      ;;
      ;; handle long split-up propstrings. this part is kind of ugly.
      (cond 
       ;;
       ;; cond: not currently in long message 
       ((equal wb-message-remain 0)
	;; read length from start of message
	(setq message-length (read (substring packet 0 10)))
	;;
	;; is new message long? 
	(if ( > message-length packet-length)
	    ;; get ready for multipart message
	    (progn
	      (setq wb-message-remain (- message-length (- packet-length 10)))
	      ;; store beginning of property string
	      (setq wb-message-stringbuf (substring packet 10)))
	  ;; new message is not long, just process it
	  (setq propstring (substring packet 10))))
       ;;
       ;; cond: currently in long message				   
       (( > wb-message-remain 0)
	;; message is part of propstring, so save it
	(setq wb-message-stringbuf (concat wb-message-stringbuf packet))
	(setq wb-message-remain (- wb-message-remain packet-length))
	;; did this complete the message? 
	(if (equal wb-message-remain 0)
	    (setq propstring wb-message-stringbuf)))
       ;;
       ;; cond: never should be reached
       (( < wb-message-remain 0)
	(error (format "wb-message-remain has fallen below 0. never should happen."))))
      ;;
      propstring))


;;;; Responding to events


(defun wb-process-filter (process input)
  "Handle input from network."
  ;;
  ;;
  (if wb-global-lock 
      (wb-queue-packet input)
    (let 
	((wb-global-lock t))
      ;; acquire global lock
      ;;
      ;; buffer input
      (wb-queue-packet input)
      ;;
      ;; on first input, check on rsyncd
      (if (and (null wb-rsync-checked) (null wb-rsync-pid))
	  (wb-get-rsync-pid))
      (setq wb-rsync-checked t)
      ;;
      ;; do we have a complete message to process?
      (let* ((packet nil)
	     (propstring nil)
	     (properties nil))
	(while (setq packet (wb-unqueue-packet))
	  (when (setq propstring (wb-handle-packet packet))
	    (setq properties (read propstring))
	    (wb-execute-plist process properties)
	    (wb-reflect process properties)))))))


(defun wb-process-sentinel (process event)
  "Handle connection and disconnection of users."
  ;;
  (wb-console-notify (concat ">>> " event))
  ;;
  ;; add new user
  (when (not (member process wb-users))
    (wb-add-user process)
    (wb-set-user-property process :ip (process-contact process :remote)))
  ;;
  ;; remove dead user
  (when (string= "closed" (process-status process))
    (let ((nick (wb-get-user-property process :nick)))
      (wb-remove-user process)
      (wb-message process :action "part" :nick nick))))


(defun wb-execute-plist (process plist)
  "Execute command coming from PROCESS within PLIST."
  (let* ((action (plist-get plist :action)))
    ;;
    ;; take action based on message :action type
    (cond 
     ;;
     ;; cond: identification 
     ((string= action "identify")
      (when wb-is-server-p
	(let ((nick (plist-get plist :nick)))
	  (wb-set-user-property process :nick nick)
	  (wb-message process :action "join" :nick nick))))
     ;;
     ;; cond: file sharing
     ((string= action "share")
      (let ((filename (plist-get plist :resource))
	    (nick (plist-get plist :nick))
	    (ip (plist-get plist :ip)))
	(wb-console-insert 
	 (button-to-string (format "<<< %s is sharing file %s"
				   nick filename)
			   `(lambda (button)
			      (wb-get-file ,filename ,ip))))))
     ;;
     ;; cond: chat messages
     ((string= action "chat")
      (let ((nick-part (propertize (concat (plist-get plist :nick) ">> ")
				   'face font-lock-keyword-face))
	    (text-part (plist-get plist :text))
	    (beg nil))
	;;
	;; buttonize URL, if any
	(if (setq beg (string-match "\\(http://[^ \n<>]+\\)" text-part))
	    (let* ((pre-url  (substring text-part 0 beg))
		   (url      (match-string 1 text-part))
		   (post-url (substring text-part (match-end 0)))
		   (button   (button-to-string url 
					       `(lambda (button)
						  (browse-url ,url)))))
	      (wb-console-insert (concat nick-part pre-url button post-url)))
	  (wb-console-insert (concat nick-part text-part)))))
     ;;
     ;; cond: normal buffer sharing
     ((string= action "buffer")
      (let ((bufname (wb-name-shared-buffer (plist-get plist :buffer-name)))
	    (buftext (plist-get plist :buffer-text)))
	(wb-replace-shared-buffer bufname buftext)
	(wb-console-insert 
	 (button-to-string (format "<<< %s has sent buffer %s"
				   (plist-get plist :nick) bufname)
			   `(lambda (button)
			      (switch-to-buffer-other-window ,bufname))))))
     ;;
     ;; cond: whiteboard locking
     ((string= action "lock")
      (let ((nick (plist-get plist :nick)))
	(setq wb-locked 'other)
	(setq wb-lock-user nick)
	(wb-force-lock)
	(wb-console-notify (format ">>> whiteboard locked by %s"
				   nick))))
	
     ;;       
     ;; cond: whiteboard unlocking
     ((string= action "unlock")
      (setq wb-locked nil)
      (setq wb-lock-user nil)
      (wb-force-unlock)
      ;; update with new text
      (wb-replace-shared-buffer "whiteboard" 
				(plist-get plist :text))
      (wb-console-insert (propertize ">>> whiteboard unlocked."
				     'face
				     font-lock-function-name-face)
			 t))
     ;;
     ;; cond: user join
     ((string= action "join")
      (wb-console-notify (format ">>> %s has joined." 
				 (plist-get plist :nick))))
     ;;
     ;; cond: user part
     ((string= action "part")
      (wb-console-notify (format ">>> %s has parted." 
				 (plist-get plist :nick))))
     ;;
     ;; end of cond
     )))
 

;;;; Whiteboard buffer


(defvar wb-whiteboard nil "Shared interactive buffer.")

(defvar wb-locked nil "Locking status of whiteboard.  If 'other, other
user is locking whiteboard. If 'me, then I am locking it.")

(defvar wb-lock-user nil "User who is locking whiteboard.")

(defvar whiteboard-mode-map nil)
(if whiteboard-mode-map 
    ()
  (setq whiteboard-mode-map (make-sparse-keymap))
  (define-key whiteboard-mode-map [(control c)(control c)] 'wb-get-or-release-lock)
  (define-key whiteboard-mode-map [(control c)(control b)] 'backward-button)
  (define-key whiteboard-mode-map [(control c)(control f)] 'forward-button))


(define-minor-mode whiteboard-mode
  "Toggle whiteboard mode. Activates or deactivates the whiteboard keymap.
With prefix argument, turn whiteboard-mode on if positive, else off.
  Use C-c C-c to lock and unlock the whiteboard buffer."
  nil " WB" whiteboard-mode-map
  ())


(defun wb-make-whiteboard ()
  (setq wb-whiteboard (get-buffer-create "whiteboard"))
  (setq wb-locked nil)
  (with-current-buffer wb-whiteboard
    (whiteboard-mode 1)))


(defun wb-get-or-release-lock ()
  (interactive)
  (cond ((equal wb-locked 'other)
	 (error "Cannot lock whiteboard. Other user is editing."))
	;; unlock it 
	((equal wb-locked 'me)
	 (setq wb-locked nil)
	 (toggle-read-only -1)
	 (wb-message wb-process
		     :action "unlock"
		     :nick wb-nick
		     :text
		     (buffer-substring-no-properties (point-min) (point-max)))
	 (wb-console-insert (propertize "<<< You unlocked the whiteboard."
					'face font-lock-function-name-face)
			    t))
	;; lock it
	((equal wb-locked nil)
	 (setq wb-locked 'me)
	 (toggle-read-only -1)
	 (wb-message wb-process :action "lock" :nick wb-nick)
	 (wb-console-notify "<<< You locked the whiteboard."))

	((t)
	 (error "code should not be reached."))))


(defun wb-force-lock ()
  (with-current-buffer wb-whiteboard
    (toggle-read-only 1)))

  
(defun wb-force-unlock ()
  (with-current-buffer wb-whiteboard
    (toggle-read-only -1)))



;;;; Interactive sharing functions


(defun wb-share-file (filename)
  "Share a file from your whiteboard directory."
  (interactive "fFilename:\n")
  (let* ((fnnd (file-name-nondirectory filename))
	 (log-message (propertize 
		      (format "<<< You are sharing file %s" fnnd)
		      'face font-lock-comment-face)))
    (wb-console-insert log-message)
    (wb-message wb-process :action "share" :nick wb-nick :ip wb-local-ip :resource fnnd)))


(defun wb-share-buffer (buffer)
  "Send a buffer to the other node."
  (interactive "bBuffer: \n")
  (let ((oldbuf (current-buffer))
	(log-message (propertize
		      (format "<<< You have sent buffer %s" buffer)
		      'face font-lock-comment-face)))
    (wb-console-insert log-message)
    (switch-to-buffer buffer)
    (wb-message wb-process
		:action "buffer" 
		:nick wb-nick
		:buffer-name (buffer-name) 
		:buffer-text (buffer-substring-no-properties
			      (point-min) (point-max)))
    (switch-to-buffer oldbuf)))

  
(defun wb-share-current-buffer ()
  "Send current buffer to the other node."
  (interactive)
  (wb-share-buffer (current-buffer)))


;;;; Other sharing-related functions


(defun wb-name-shared-buffer (name)
  (if (string-match "^WB:" name)
      name
    (concat "WB:" name)))


(defun wb-replace-shared-buffer (buffer-name text)
  (let ((buf (get-buffer-create buffer-name)))
    (with-current-buffer buf
      (delete-region (point-min) (point-max))
      (insert text))))
	

;;;; Configuring and running rsync


(defvar wb-rsync-pid nil "PID for rsync daemon.")
(defvar wb-rsync-checked nil "Whether we've checked on rsync.")


(defun wb-configure-rsync ()
  "Write configuration file for rsync." 
  (let ((config (format (concat 
			 "port = %d \n"
			 "log file = %s \n"
			 "pid file = %s \n"
			 "\n"
			 "[wb] \n"
			 "path = %s \n"
			 "use chroot = false \n")
			(+ 1 wb-base-port) 
			(format "%s/%s" (getenv "HOME") ".wb.rsyncd.log")
			(format "%s/%s" (getenv "HOME") ".wb.rsyncd.pid")
			(expand-file-name wb-directory))))
    (with-temp-buffer
      (insert config)
      (write-file "~/.wb.rsyncd.conf"))))


(defun wb-start-rsync ()
  (call-process "rsync" nil nil nil
		"--daemon"
		(format "--config=%s/.wb.rsyncd.conf" (getenv "HOME"))))
	      

(defun wb-kill-rsync ()
  (when (numberp wb-rsync-pid) 
    (call-process "kill" nil nil nil (format "%d" wb-rsync-pid)))
  (setq wb-rsync-pid nil)
  (setq wb-rsync-checked nil))


(defun wb-get-rsync-pid ()
    (let ((pid-file "~/.wb.rsyncd.pid"))
      (if (file-exists-p pid-file)
	  (with-temp-buffer
	    (insert-file-contents pid-file)
	    (goto-char (point-min))
	    (setq wb-rsync-pid (read (current-buffer)))
	    (message (format "Rsync daemon on PID %d" wb-rsync-pid)))
	(message "Failure in starting rsync daemon. You will not be able to share files."))))
      

(defun wb-get-file (filename ip)
  (interactive "sFilename\n")
  (wb-console-notify (format "Downloading %s from node %s" filename ip))
  (let ((process nil))
    (setq process 
	  (start-process "wb-transfer" "*wb-transfer*" "rsync" 
			 "-avz"
			 "--filter"
			 (format "+ %s" filename)		
			 "--filter"
			 "- *"
			 "--port"
			 (format "%d" (+ 1 wb-base-port))
			 (concat ip "::wb")
			 (expand-file-name wb-directory)))
    (set-process-sentinel process 'wb-rsync-sentinel)
    (process-put process :filename filename)))


(defun wb-rsync-sentinel (process event)
  (when (string= "exit" (process-status process))
    (let ((status (process-exit-status process))
	  (filename (process-get process :filename))
	  (message nil))
      (if (equal 0 status)
	  (setq message (format "File %s downloaded successfully."
				  filename))
	(setq message (format "File %s failed to download."
			      filename)))
      (wb-console-notify message))))



(provide 'whiteboard)
;;; whiteboard.el ends here
